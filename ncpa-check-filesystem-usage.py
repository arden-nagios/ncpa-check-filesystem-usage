import os
import argparse
import shutil
import subprocess
import re
import sys
import math

#nagios states
STATE_OK=0
STATE_WARNING=1
STATE_CRITICAL=2
STATE_UNKNOWN=3

# Path to command definition
DF_COMMAND = "/bin/df"

def disk_check(warning_val,critical_val) -> dict:
    """
    Monitors all of the locally attached file systems on
    the host and issues an alert when any one of them is
    above the desired usage threshold.
    :param entry: string containing flags "-w" or "-c"
    :type entry: str
    """
    output = subprocess.check_output([
        DF_COMMAND,
        "-x", "tmpfs",
        "-x", "devtmpfs",
        '--output=source,target,fstype'])

    disks = {}
    maxUsage= 0
    critical = 0
    warning = 0
    ok = 0
    maxName = ""
    maxMount_Point = ""
    maxUsed = 0
    maxTotal = 0

    for line in output.decode("UTF-8").splitlines():
        line_pattern = re.compile(r'^([\w\-\/.-]+)\s+([\w\-\/.-]+)\s+([\w]+)$')
        match = line_pattern.fullmatch(line)

        if not match:
            continue

        disk = {}
        mount_point = match.group(2) #what do these numbers mean
        disk['fileSystem'] = match.group(1)
        disk['type'] = match.group(3)
        disk['target'] = match.group(2)

        r = shutil.disk_usage(disk['target'])
        used_val = r.used/(1024*1024*1024)
        total_val = r.total/(1024*1024*1024)
        disk['usage'] = int(math.ceil((used_val/total_val) * 100.0))

        #finding maxUsagedrive
        if maxUsage< disk['usage']:
            maxUsage= disk['usage']
            maxName = disk['target']
            maxMount_Point = match.group(2)
            maxUsed = int(math.ceil(r.used/(1024*1024*1024)))
            maxTotal = int(math.ceil(r.total/(1024*1024*1024)))

        # if statements to categorize drives
        if disk['usage'] >= critical_val:
            critical = critical + 1
        elif disk['usage'] >= warning_val:
            warning = warning + 1
        else:
            ok = ok + 1
        disks[mount_point] = disk


    #Output statements
    if critical > 0:
        strmsg = "CRIT: {critState}c/{warnState}w/{okState}o - '{fname}' is {dUsage}'%' used! ({used}/{total} GiB)".format(
                critState = critical,
                warnState = warning,
                okState = ok,
                fname = maxMount_Point,
                dUsage = maxUsage,
                used = maxUsed,
                total = maxTotal)
        state = STATE_CRITICAL
    elif warning > 0:
        strmsg = "WARN: {critState}c/{warnState}w/{okState}o - '{fname}' is {dUsage}'%' used! ({used}/{total} GiB)".format(
                critState = critical,
                warnState = warning,
                okState = ok,
                fname = maxMount_Point,
                dUsage = maxUsage,used=maxUsed,
                total = maxTotal)
        state = STATE_WARNING
    else:
        strmsg = "OK: {critState}c/{warnState}w/{okState}o - all disks are OK!".format(
                critState = critical,
                warnState = warning,
                okState = ok)
        state = STATE_OK

    result = {}
    result["message"] = strmsg
    result["code"] = state

    return result

def main():
    DESC_TEXT = "Monitors storage devices on server and issues warnings for devices exceeding threshold"
    PARSER = argparse.ArgumentParser(description=DESC_TEXT)

    #Issue warning flag
    PARSER.add_argument(
        "-w",
        "--warning",
        dest='warning_val',
        required=True,
        help="Threshold to issue warning alert"
        )
    #Issue critical flag
    PARSER.add_argument(
        "-c",
        "--critical",
        dest='critical_val',
        required=True,
        help="Threshold to issue critical alert"
    )
    args = PARSER.parse_args()

    warning_val = int(args.warning_val)
    critical_val = int(args.critical_val)

    try:
        result = disk_check(warning_val,critical_val)
    except subprocess.CalledProcessError as error:
        strmsg = "UNKNOWN: failed to run df command"
        state = STATE_UNKNOWN
    except FileNotFoundError as e:
        strmsg = "UNKNOWN: shutil error '{emsg}'".format(emsg=e.strerror)
        state = STATE_UNKNOWN
    except:
        strmsg = "UNKNOWN: failed to run disk check"
        state = STATE_UNKNOWN

    print(result["message"])
    sys.exit(result["code"])

if __name__ == "__main__":
    main()
