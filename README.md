# NCPA Check Filesystem Usage

Monitors all of the locally attached file systems on the host and issues an alert when any one of them is above the desired usage threshold.

## Overview

### Arguments
The script accepts the following arguments:

| Flag | LongFlag | Description |
|------|----------|-------------|
| `-w` | `--warning` | Threshold to issue warning alert |
| `-c` | `--critical` | Threshold to issue critical alert |


### Output

If multiple disks exceed a threshold the message will be chosen based on the following logic:
* If any file system is above the critical threshold the alert will have STATUS equal to "CRIT", else it will be OK.
* The message will indicate the number of filesystems above CRIT, the number above WARN, and the number which are OK
* Finally, the text of the message will list the details of the file system with the highest utilization.

Messages take the following format:

```bash
${STATUS}: ${CRIT}C/${WARN}W/${OK}O - '${MOUNTPOINT}' is ${USAGE}% used! (${SIZE}/${USAGE} ${UNIT})
```

### Examples

The following examples will reference a system with the following disks using the flags `-w 75 -c 95`.

| Filesystem | Size | Mount Point |
|------------|------|-------------|
| `/dev/mapper/rhel75--base-root` | 30G | `/` |
| `/dev/mapper/mpathb2`           | 1014M | `/boot` |
| `/dev/mapper/sap-daa`           | 3.0G | `/usr/sap/DAA` |
| `/dev/mapper/rhel75--base-home` | 10G | `/home` |
| `/dev/mapper/sap-ecp`           | 16G | `/usr/sap/ECP` |


#### 1 - OK

If all disk utilizations are below the target threshold the output should match the following:

```
OK: 0C/0W/5O - all disks are OK!
```

#### 2 - One Warning

This example assumes the following usage percentages:

| Filesystem | Usage |
|------------|-------|
| `/dev/mapper/rhel75--base-root` | 55% |
| `/dev/mapper/mpathb2`           | 25% |
| `/dev/mapper/sap-daa`           | 4%  |
| `/dev/mapper/rhel75--base-home` | 83% |
| `/dev/mapper/sap-ecp`           | 26% |

The following output should be produced:

```
WARN: 0C/1W/4O - '/home' is 83% used! (8.3/10 GiB)
```

#### 2 - Critical and multiple warnings

This example assumes the following usage percentages:

| Filesystem | Usage |
|------------|-------|
| `/dev/mapper/rhel75--base-root` | 55% |
| `/dev/mapper/mpathb2`           | 77% |
| `/dev/mapper/sap-daa`           | 4%  |
| `/dev/mapper/rhel75--base-home` | 83% |
| `/dev/mapper/sap-ecp`           | 100% |

The following output should be produced:

```
CRIT: 1C/2W/3O - '/usr/sap/ECP' is 100% used! (16/16 GiB)
```


## Execution

### Manual

Executing this script from a directory on the server should produce output similar to the following:

```bash
./check_filesystem_usage.py -c 95 -w 75
```

In this case the script will alert if any locally attached disk exceeds 75% disk space utilization. If the utilization is below 95% it will be a warning, otherwise it will be a hard alert.

### Nagios Config / Execution

This depends on your nagios environment, however, the check command line should be similar to the details below

```bash
/usr/local/nagios/libexec/check_ncpa.py -H $HOSTADDRESS$ -t $ARG1$ -M "plugins/check_filesystem_usage.py" -a "-w $ARG2$ - $ARG3$"
```
